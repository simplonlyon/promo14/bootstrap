# bootstrap

## Exo 1 Grid [html](first-grid.html) / [css](css/first-grid.css)
Créer un fichier first-grid.html et charger dedans le css et le js (pas utile dans ce cas, mais c'est histoire de...) de bootstrap
1. Faire une structure avec un main qui contiendra 6 articles qui auront tous une bordure
2. Rajouter les div avec les classes bootstrap pour faire que ça ressemble à ça sur desktop
![version desktop](wireframes/desktop.png)
3. Puis, rajouter de nouvelles classes col pour que ça ressemble à ça sur tablet
![version tablet](wireframes/tablet.png)
4. Enfin, faire que sur mobile il n'y ait pu qu'un article par ligne


## Exo 2 Grid [html](exo-bootstrap.html) / [css](css/exo-bootstrap.css)
Créer le html css avec bootstrap et sa grid pour reproduire ces 3 wireframes :

### Desktop
![version desktop](wireframes/exo-bootstrap-desktop.png)
### Tablet
![version tablet](wireframes/exo-bootstrap-tablet.png)
### Mobile
![version mobile](wireframes/exo-bootstrap-mobile.png)